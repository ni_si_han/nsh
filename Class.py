class Student:
    name = ""
    age = ""
    def __init__(self, name, age):
        Student.name = name
        Student.age = age
        Student.school = "besti"
        print("姓名："+Student.name+" 年龄："+Student.age+" 学校："+Student.school)
    def hometown(self):
        print("籍贯为：浙江")
class Student2(Student):
    def hometown(self):
        print("籍贯为：山东")
class Student3(Student):
    def hometown(self):
        print("籍贯为：陕西")
class Student4(Student):
    def hometown(self):
        print("籍贯为：大连")
student1 = Student("nsh","20")
student1.hometown()
student2 = Student2("lsl","18")
student2.hometown()
student3 = Student3("fyc","19")
student3.hometown()
student3 = Student4("ysf","20")
student3.hometown()

