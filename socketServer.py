import socket, threading
client_list = []
def server_target(sever_socket):
    while True:
        content = sever_socket.recv(2048).decode('UTF-8')
        if content is not None:
            print("已经接受到数据：%s" % content)
            for cl in client_list:
                cl.send(content.encode('UTF-8'))
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
s.bind(('127.0.0.1', 8001))
s.listen()
while True:
    c, addr = s.accept()
    print('接收到的服务端有：',addr)
    client_list.append(c)
    threading.Thread(target=server_target, args=(c,)).start()
